from PySide2.QtWidgets import QApplication
from tasktools.taskloop import coromask, renew, simple_fargs
from termcolor import colored, cprint
from gnsocket.gn_socket import GNCSocket
from gnsocket.conf.socket_conf import TEST_TEXT
import sys
import asyncio
import uvloop
import socket
from gnsocket.socket_client import GNCSocketClient
import functools
import concurrent.futures
from multiprocessing import Manager, Queue

from networktools.ssh import bridge, kill
from networktools.ports import used_ports, get_port
from networktools.library import gns_loads, gns_dumps

from gui_client import ClientMicroServiceGUI

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())


def gprint(text):
    msg = colored(text, 'green', attrs=['reverse', 'blink'])
    print(msg)


def bprint(text):
    msg = colored(text, 'blue', attrs=['reverse', 'blink'])
    print(msg)


def rprint(text):
    msg = colored(text, 'red', attrs=['reverse', 'blink'])
    print(msg)


def socket_exception(ex, gs, idc):
    bprint("Excepción en socket")
    bprint(gs)
    rprint("Id socket client -> %s" % idc)
    raise ex


class InputOperacion:
    """
    Esta clase no funciono pk entrega un EOF al INPUT

    Se prodría arreglar leyendo de un archivo una serie de datos linea a linea
    comando,a,b
    """
    RESULTADOS = {
        'suma': "a+b={answer}",
        'multiplica': "a*b={answer}"
    }

    def __init__(self, qn2t, qt2n):
        self.qt2n = qt2n
        self.qn2t = qn2t

    async def operacion(self):
        await asyncio.sleep(.5)
        # queue msg from socket or network
        queue_in = self.qn2t
        # queue msg send answer
        queue_out = self.qt2n
        if not queue_in.empty():
            for i in range(queue_in.qsize()):
                msg = queue_in.get()
                bprint("=====RESULTADO=====")
                rprint(msg)
                answer = msg.get('answer')
                action = msg.get('action')
                result = self.RESULTADOS.get(action)
                gprint(result.format(answer=answer))
        # preguntar accion
        accion = input("Ingresa la accion {suma, multiplica}")
        await asyncio.sleep(1)
        if accion in {'suma', 'multiplica'}:
            a = input("Ingresa *a*")
            b = input("Ingresa *b*")
            args = (float(a), float(b))
            msg = {'action': accion, 'args': args}
            queue_out.put(msg)
        else:
            print("Intenta de nuevo :>")

    def operacion_task(self):
        loop = asyncio.get_event_loop()
        # gprint("Gestionando mensajes en engine")
        try:
            args = []
            # Create instances
            rprint(args)
            task = loop.create_task(
                coromask(
                    self.operacion,
                    args,
                    simple_fargs)
            )
            task.add_done_callback(
                functools.partial(
                    renew,
                    task,
                    self.operacion,
                    simple_fargs)
            )
            if not loop.is_running():
                loop.run_forever()
        except Exception as ex:
            print("Error o exception que se levanta con %s" %
                  format("Error en task :S"))
            print(ex)
            raise ex


def run_interfaz(qn2t, qt2n):
    print("RUNNING GUI============================")
    app = QApplication(sys.argv)
    loop = asyncio.get_event_loop()
    asyncio.set_event_loop(loop)
    q = {}
    ex = ClientMicroServiceGUI(qn2t, qt2n, **q)
    ex.show()
    sys.exit(app.exec_())
    if not loop.is_running():
        loop.run_forever()


if __name__ == "__main__":
    workers = 2
    mode = 'client'
    host_port = 7777
    host = socket.gethostbyname(socket.gethostname())
    address = (host, host_port)
    bprint("Direccion a socket")
    rprint(address)
    with concurrent.futures.ProcessPoolExecutor(workers) as executor:
        loop = asyncio.get_event_loop()
        # Manger to share data strucs
        manager = Manager()
        queue_n2t = manager.Queue()
        # network->terminal
        queue_t2n = manager.Queue()
        client_socket = GNCSocketClient(
            queue_n2t, queue_t2n, address=address,
            callback_exception=socket_exception)
        try:
            t1 = loop.run_in_executor(
                executor,
                functools.partial(run_interfaz,
                                  queue_n2t,
                                  queue_t2n)
            )
            t2 = loop.run_in_executor(
                executor,
                client_socket.socket_task
            )
            tasks = [t1, t2]
            loop.run_until_complete(asyncio.gather(*tasks))
        except Exception as e:
            raise e
