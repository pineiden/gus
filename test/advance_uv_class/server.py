from networktools.ssh import clean_port
from multiprocessing import Manager, Queue
import asyncio
from gnsocket.gn_socket import GNCSocket
from gnsocket.conf.socket_conf import TEST_TEXT
from tasktools.taskloop import coromask, renew, simple_fargs
import functools
from termcolor import colored, cprint
import uvloop
import socket
import concurrent.futures

from gnsocket.socket_server import GNCSocketServer

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

def gprint(text):
    msg = colored(text, 'green', attrs=['reverse', 'blink'])
    print(msg)


def bprint(text):
    msg = colored(text, 'blue', attrs=['reverse', 'blink'])
    print(msg)


def rprint(text):
    msg = colored(text, 'red', attrs=['reverse', 'blink'])
    print(msg)


def socket_exception(ex, gs, idc):
    bprint("Excepción en socket")
    bprint(gs)
    rprint("Id socket client -> %s" % idc)
    raise ex


def suma(a, b):
    return a+b


def multiplica(a, b):
    return a*b


class Operacion:

    OPS = {
        'sumar': suma,
        'multiplicar': multiplica
    }

    def __init__(self, qn2t, qt2n):
        self.qt2n = qn2t
        self.qn2t = qt2n

    async def operacion(self):
        await asyncio.sleep(.5)
        # queue msg from socket or network
        queue_in = self.qn2t
        # queue msg send answer
        queue_out = self.qt2n
        if not queue_in.empty():
            for i in range(queue_in.qsize()):
                msg = queue_in.get()
                bprint("Mesnaje recibido")
                bprint(msg)
                action = msg.get('dt').get('action', 'suma')
                rprint("Options")
                rprint(self.OPS)
                bprint("action %s" % action)
                op = self.OPS.get(action)
                args = msg.get('dt').get('args',
                                         dict(
                                             zip(
                                                 ('a', 'b'),
                                                 (0, 0)
                                             )
                                         )
                                         )
                a = args.get('a', 0)
                b = args.get('b', 0)
                print("Operando::: fn -> a -> b", op, a, b)
                resp = op(a, b)
                msg.update({'answer': resp})
                queue_out.put(msg)

    def operacion_task(self):
        loop = asyncio.get_event_loop()
        gprint("Gestionando operacion ::::")
        try:
            args = []
            # Create instances
            rprint(args)
            task = loop.create_task(
                coromask(
                    self.operacion,
                    args,
                    simple_fargs)
            )
            task.add_done_callback(
                functools.partial(
                    renew,
                    task,
                    self.operacion,
                    simple_fargs)
            )
            if not loop.is_running():
                loop.run_forever()
        except Exception as ex:
            print("Error o exception que se levanta con %s" %
                  format("Error en task :S"))
            print(ex)
            raise ex


"""
 Init socket server
"""

if __name__ == "__main__":
    workers = 2
    host_port = 7777
    host = socket.gethostbyname(socket.gethostname())
    address = (host, host_port)
    bprint("Direccion server socket")
    rprint(address)
    with concurrent.futures.ProcessPoolExecutor(workers) as executor:
        loop = asyncio.get_event_loop()
        # Manger to share data strucs
        manager = Manager()
        queue_n2t = manager.Queue()
        # network->terminal
        queue_t2n = manager.Queue()
        operacion = Operacion(queue_n2t, queue_t2n)
        clean_port(host_port)
        server_socket = GNCSocketServer(
            queue_n2t, queue_t2n, address=address,
            callback_exception=socket_exception)
        try:
            t1 = loop.run_in_executor(
                executor,
                operacion.operacion_task
            )
            t2 = loop.run_in_executor(
                executor,
                server_socket.socket_task
            )
            tasks = [t1, t2]
            loop.run_until_complete(asyncio.gather(*tasks))
        except Exception as e:
            raise e
