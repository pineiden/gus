from gnsocket.gn_socket import GNCSocket
from gnsocket.conf.socket_conf import TEST_TEXT
import sys
import asyncio
from asyncio import wait_for, shield

from networktools.ssh import bridge, kill
from networktools.ports import used_ports, get_port
from networktools.library import gns_loads, gns_dumps
from datetime import datetime, timedelta
from networktools.colorprint import gprint, bprint, rprint

if __name__ == "__main__":
    mode = 'client'
    host_port=6677
    host='127.0.0.1'
    opts = {'timeout':10, 'raise_timeout':False}    
    gs = GNCSocket(mode=mode, host=host, port=host_port,**opts)
    user='geodesia'
    loop = asyncio.get_event_loop()
    gs.set_loop(loop)
    # gs.accept()
    msg = TEST_TEXT

    # Testing message generator
    print("Status %s" %gs.status)
    for m in gs.generate_msg(msg):
        print(m.decode('utf-8'))
    # gs.send_msg(msg)
    # print(gs.conn)
    # Testing communicate
    # gs.accept()
    # print(gs.conn)
    async def send_msg(loop):
        idc = ""
        create_client = True
        while True:
            if create_client:
                 try:
                     await asyncio.sleep(5)
                     idc = await gs.create_client()
                     writer = gs.clients[idc]['writer']
                     reader = gs.clients[idc]['reader']
                     create_client = False
                 except (ConnectionResetError, ConnectionAbortedError) as conn_error:
                     gs.logger.exception("Excepción por desconexión %s, mode %s"%(
                         conn_error,gs.mode))
                     await asyncio.sleep(2)
                     if idc:
                         await gs.close(idc)
                     create_client = True
                     continue
                 except Exception as x:
                     print("Error al crear cliente: %s " %x)
                     await asyncio.sleep(10)
                     create_client = True
                     continue
            elif idc and not create_client:
                 await asyncio.sleep(1)
                 try:
                     msg = ''
                     while not msg=='<END>':
                         try:
                             rprint("reading msg->%s"%msg)
                             pre = datetime.now()
                             recv =await gs.recv_msg(idc)
                             delta = datetime.now()-pre
                             msg = recv
                             rprint("Msg desde server->%s"%msg)
                             bprint("received in-> %s" %delta.total_seconds())
                         except (ConnectionResetError, ConnectionAbortedError) as conn_error:
                             gs.logger.exception("Excepción por desconexión %s, mode %s"%(
                                 conn_error,gs.mode))
                             await asyncio.sleep(2)
                             await gs.close(idc)
                             create_client = True
                             continue
                         except asyncio.TimeoutError as te:
                             gs.logger.exception("mode %s, Tiempo fuera al leer en readbytes, %s"%(
                                 mode,te))
                             await gs.close(idc)
                             await asyncio.sleep(2)
                             create_client = True
                             continue                            
                         #print("Crude msg", msg)
                         if msg != '<END>' and msg is not None:
                             print(msg)
                         if msg == '<END>':
                             print("Closing msg on client")
                             break

                     this_msg = input("->")
                     #
                     if this_msg != "":
                         print("SEND :", this_msg)
                         try:
                             await gs.send_msg(this_msg, idc)
                         except (ConnectionResetError, ConnectionAbortedError) as conn_error:
                             rprint("Error de conexion on top, desconexion")
                             gs.logger.exception("Excepción por desconexión %s, mode %s"%(
                                 conn_error,gs.mode))
                             await asyncio.sleep(10)
                             await gs.close(idc)
                             bprint("Set create client to true")
                             create_client = True
                             continue
                         except asyncio.TimeoutError as te:
                             rprint("Error de timeout")
                             gs.logger.exception("mode %s, Tiempo fuera al leer en readbytes, %s"%(
                                 mode,te))
                             await asyncio.sleep(10)
                             await gs.close(idc)
                             create_client = True
                             continue                            
                         print("Ya enviado: %s" % this_msg)
                         if this_msg == "DONE":
                             print("Cerrando server")
                 except KeyboardInterrupt as k:
                     if writer.can_write_eof():
                         writer.write_eof()
                     print("Cerrando con kb")
                     await gs.close(idc)
                     sys.exit()
                 except Exception as ex:
                     print("Error en conexion a puerto mediante bridge")
                     print("Error de tipo %s" %ex)
                     raise ex
    #try:
    loop.run_until_complete(send_msg(loop))
    #except Exception as exec:
    #    print("Error %s " % exec)
    #    kill(ssh_bridge)
    loop.close()
    print("-" * 20)
    print("Apagando")
    print("Listo!")
