from gnsocket.gn_socket import GNCSocket
from gnsocket.conf.socket_conf import TEST_TEXT
import sys
import asyncio
import uvloop
import socket


from networktools.ssh import bridge, kill
from networktools.ports import used_ports, get_port
from networktools.library import gns_loads, gns_dumps

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

if __name__ == "__main__":
    mode = 'client'
    host_port = 6688
    host = socket.gethostbyname(socket.gethostname())
    address = (host, host_port)
    ssl_data = {
        'ssl': True,
        'set_certificates': {
            'cafile': './socket_serv.crt',
        }
    }
    gs = GNCSocket(mode=mode, address=address, **ssl_data)
    user ='geodesia'
    print(address)
    gs.set_address(address)
    loop = asyncio.get_event_loop()
    gs.set_loop(loop)
    # gs.accept()
    msg = TEST_TEXT

    # Testing message generator
    print("Status %s" %gs.status)
    for m in gs.generate_msg(msg):
        print(m.decode('utf-8'))
    # gs.send_msg(msg)
    # print(gs.conn)
    # Testing communicate
    # gs.accept()
    # print(gs.conn)
    async def send_msg(loop):
        try:
            await asyncio.sleep(5)
            idc = await gs.create_client()
            writer = gs.clients[idc]['writer']
            reader = gs.clients[idc]['reader']
        except Exception as x:
            print("Error al crear cliente: %s " %x)
            raise x
        while True:
            await asyncio.sleep(1)
            try:
                msg=''
                while not msg == '<END>':
                    msg = await gs.recv_msg(idc)
                    #print("Crude msg", msg)
                    if msg != '<END>' and msg is not None:
                        print(msg)
                    if msg == '<END>':
                        print("Closing msg on client")
                        break

                this_msg = input("->")
                #
                if this_msg != "":
                    print("SEND :", this_msg)
                    await gs.send_msg(this_msg, idc)
                    await gs.send_msg('<END>', idc)

                    print("Ya enviado: %s" % this_msg)
                    if this_msg == "DONE":
                        print("Cerrando server")
            except KeyboardInterrupt as k:
                if writer.can_write_eof():
                    writer.write_eof()
                print("Cerrando con kb")
                gs.close()
                sys.exit()
            except Exception as exec:
                print("Error en conexion a puerto mediante bridge")
                print("Error de tipo %s" %exec)
                raise exec
    #try:
    loop.run_until_complete(send_msg(loop))
    #except Exception as exec:
    #    print("Error %s " % exec)
    #    kill(ssh_bridge)
    loop.close()
    print("-" * 20)
    print("Apagando")
    print("Listo!")
