import asyncio
from gnsocket.gn_socket import GNCSocket
from gnsocket.conf.socket_conf import TEST_TEXT
from tasktools.taskloop import TaskLoop
import functools
from termcolor import colored, cprint
import socket

def gprint(text):
    msg = colored(text, 'green', attrs=['reverse', 'blink'])
    print(msg)


def bprint(text):
    msg = colored(text, 'blue', attrs=['reverse', 'blink'])
    print(msg)


def rprint(text):
    msg = colored(text, 'red', attrs=['reverse', 'blink'])
    print(msg)


if __name__ == "__main__":

    mode = 'server'
    port = 6666
    host = socket.gethostbyname(socket.gethostname())
    gs = GNCSocket(mode=mode, host=host, port=port)
    print(gs.address)
    msg = TEST_TEXT
    idc = "X"
    # Testing message generator
    print("Status %s" % gs.status)
    for m in gs.generate_msg(msg):
        print(m.decode('utf-8'))
    # gs.send_msg(msg)
    # gs.send_msg(msg)
    # print(gs.conn)
    # Testing communicate
    print(gs)
    print("Entrando a loop")
    loop = asyncio.get_event_loop()
    gs.set_loop(loop)
    tsleep = 1  # 1 second sleep by some coroutines
    # Create coroutines

    async def sock_read(queue, idc,**kwargs):
        bprint("Sock read")
        try:
            datagram = await gs.recv_msg(idc)
            bprint("msg recibido")
            if not datagram == '' and \
               datagram != "<END>":
                bprint("Recibido en server")
                bprint(datagram)
                await queue.put(datagram)
            await asyncio.sleep(tsleep)
            # print(msg_tot)
        except Exception as ex:
            gs.set_status('OFF')
            raise ex
        return (queue, idc), {}

    async def sock_write(queue, idc, **kwargs):
        rprint("Sock write")
        # read async queue
        try:
            if not queue.empty():
                for q in range(queue.qsize()):
                    msg = await queue.get()
                    print(f"Enviando msg {msg}")
                    await gs.send_msg(msg, idc)
                await gs.send_msg("<END>", idc)
                rprint("Msg enviado")
            await asyncio.sleep(tsleep)
        except Exception as ex:
            gs.set_status('OFF')
            raise ex
        return (queue, idc), {}


    async def socket_io(reader, writer):
        queue = asyncio.Queue()
        idc = await gs.set_reader_writer(reader, writer)
        # First time welcome
        welcome = "Welcome to socket"
        rprint(welcome)
        await gs.send_msg(welcome, idc)
        await gs.send_msg("<END>", idc)
        # task reader
        try:
            args = [queue, idc]
            task1 = TaskLoop(
                    sock_read,
                    args,{},
            )
            # task write
            task2 = TaskLoop(
                    sock_write,
                    args,{},
            )
            task1.create()
            task2.create()
        except Exception as ex:
            raise ex
    try:
        future = loop.create_task(gs.create_server(socket_io, loop))
        loop.run_forever()
    except Exception as e:
        raise e
    loop.close()
    gs.close()
