keyfile=socket_serv.key
crtfile=socket_serv.crt

openssl req -newkey rsa:2048 -nodes -keyout $keyfile \
        -x509 -days 365 -out $crtfile

echo "LLave y certificados creados"
echo "Copiar certificado desde cliente y usar: archivo.crt"
