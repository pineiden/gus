from gnsocket.socket_client import GNCSocketClient
from tasktools.taskloop import coromask, renew, simple_fargs_out
from functools import partial
import asyncio
import queue
from functools import partial 
# crear un ciclo que tome las colas
# recibe texto
# lo pasa  mayúscula
from networktools.colorprint import rprint, gprint

async def manage(queue_n2t, queue_t2n, *args, **kwargs):
    # ead n2t
    print("QN2t", queue_n2t.qsize())
    print("Qt2n", queue_t2n.qsize())
    await asyncio.sleep(2)
    if not queue_t2n.empty():
        for i in range(queue_t2n.qsize()):
            msg = queue_t2n.get()
            gprint("New msg::::")
            rprint(msg)
            queue_t2n.task_done()
    # now use queue_n2t to PUT
    await asyncio.sleep(1)
    txt = input("Dame un texto: \n")
    msg = {"texto": txt}
    queue_n2t.put(msg)
    return [queue_n2t, queue_t2n, *args], kwargs


# fn que maneja excepción en socket
def socket_exception(ex, gs, idc):
    bprint("Excepción en socket client")
    bprint(gs)
    rprint("Id socket client -> %s" % idc)
    raise ex

async def socket_task(queue_n2t, queue_t2n, address, *args, **opts):
    server_socket = GNCSocketClient(
        queue_n2t, queue_t2n, address=address,
        callback_exception=socket_exception, **opts)
    server_socket.socket_task()

if __name__ == "__main__":
    loop=asyncio.get_event_loop()
    queue_n2t=queue.Queue()
    queue_t2n=queue.Queue()
    host_port = 6677
    host = '127.0.0.1'
    address = (host, host_port)
    args = [queue_n2t, queue_t2n]

    task_1 = loop.create_task(
        coromask(
            manage,
            args, {},
            simple_fargs_out)
    )
    task_1.add_done_callback(
        partial(
            renew,
            task_1,
            manage,
            simple_fargs_out)
    )

    task_2 = loop.create_task(socket_task(queue_n2t, queue_t2n, address))

    loop.run_forever()
