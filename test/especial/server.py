from gnsocket.socket_server import GNCSocketServer
from tasktools.taskloop import coromask, renew, simple_fargs_out
from functools import partial
import asyncio
import queue
from functools import partial 
# crear un ciclo que tome las colas
# recibe texto
# lo pasa  mayúscula
import json

async def manage(queue_n2t, queue_t2n, *args, **kwargs):
    await asyncio.sleep(1)
    if not queue_t2n.empty():
        for i in range(queue_t2n.qsize()):
            msg = queue_t2n.get()
            print(msg, type(msg),msg.get('dt'),type(msg.get('dt')), )
            MSG = msg.get('dt').get('texto')
            print(MSG, type(MSG))
            print("IDC",msg.get('idc'))
            queue_n2t.put({"texto":MSG.upper(),"idc_server":msg.get('idc')})
            queue_t2n.task_done()
    return [queue_n2t, queue_t2n, *args], kwargs


# fn que maneja excepción en socket
def socket_exception(ex, gs, idc):
    bprint("Excepción en socket")
    bprint(gs)
    rprint("Id socket client -> %s" % idc)
    raise ex

async def socket_task(queue_n2t, queue_t2n, address, *args, **opts):
    server_socket = GNCSocketServer(
        queue_n2t, queue_t2n, address=address,
        callback_exception=socket_exception, **opts)
    server_socket.socket_task()

if __name__ == "__main__":
    loop=asyncio.get_event_loop()
    queue_n2t=queue.Queue()
    queue_t2n=queue.Queue()
    host_port = 6677
    host = '127.0.0.1'
    address = (host, host_port)
    args = [queue_n2t, queue_t2n]

    task_1 = loop.create_task(
        coromask(
            manage,
            args, {},
            simple_fargs_out)
    )
    task_1.add_done_callback(
        partial(
            renew,
            task_1,
            manage,
            simple_fargs_out)
    )

    task_2 = loop.create_task(socket_task(queue_n2t, queue_t2n, address))

    loop.run_forever()
