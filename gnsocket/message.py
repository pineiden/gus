from typing import Protocol, Any, Callable, Dict
from abc import abstractmethod
from dataclasses import field, dataclass
from networktools.library import (my_random_string,)


MSG_STRUCT = b"IDX hex(PAGE)/hex(TOT_PAGES) MSG hex(LEN) MSG END"
MSG_TEMPLATE = "{idx} {page}/{tot_pages} MSG {length} MSG END"
MSG_SPACES = [x.start() for x in re.finditer(b' ', MSG_STRUCT)]


class Conversor(Protocol):
    @abstractmethod
    def generate_msg(self, element):
        """
        Take an object and serialize this.
        Consider buffersize, pagination, template
        """
        pass

    @abstractmethod
    def send_msg(self):
        """
        Send a message, for example to send a serialized object
        """
        pass 

    @abstractmethod
    def recv_msg(self):
        """
        Receive the message controling pagination, buffsize, etc
        """
        pass

    @abstractmethod
    def gen_idx(self):
        """
        Generate identificator
        """
        pass

class TextToBytes(Conversor):
    def generate_msg(self, element):
        """
        Take an object and serialize this.
        Consider buffersize, pagination, template
        """
        b_msg = msg.encode(char_code)
        T_LEN = len(b_msg)
        # Cantidad de caracteres en hexadecimal
        hex_T_LEN = hextring(T_LEN)
        # Se obtiene el largo de caracteres como
        # cota superior de largo de mensaje
        # A utilizar en paginacion
        # largo del valor
        # n_char es lo mismo
        new_n = len(hex_T_LEN)
        self.n_char = new_n
        # obtener el largo base del mensaje
        self.base_len = len(self.header) + len(self.ender) + \
            len(self.msg_spaces) + self.uin + 3 * self.n_char + 1
        # n_char is the amount of chars for numbers
        # tha last  +1 is for the space after IDX
        # Largo de pedazo de mensaje a enviar por partes
        self.len_msg = self.bs - self.base_len

        assert self.len_msg >= 1, "Debes definir un buffsize de mayor tamaño"
        assert new_n + 1 < self.len_msg, "No es posible enviar mensaje"


        # Cantidad maxima de mensajes a enviar
        n_msgs = math.ceil(T_LEN / self.len_msg)
        # transformar a hex y sacar string
        hex_n_msgs = hextring(n_msgs)
        # Cantidad de paginas
        self.N_n_msgs = len(str(hex_n_msgs))
        # hex_nmsgs = str(hex(n_msgs)).split('x')[1]
        ender = self.ender.encode(char_code)
        # Se construye: hex(PAGE)/hex(TOT_PAGES) MSG hex(LEN) MSG END
        NON = "".encode(char_code)
        head_template = "{header} {page}/{tot_pages} {length} "
        msg_ender = " END"
        if n_msgs > 1:
            for i in range(n_msgs):
                # Construir header:
                # Conocer parte de msg a enviar
                # Conocer largo de msg_i
                # Cantidad carácteres largo
                step = i * self.len_msg
                this_page = hextring(i + 1)
                msg_i = b_msg[step:step + self.len_msg]
                msg_l = len(msg_i)
                # +2 erased because quoted don't go
                msg_len = msg_l + len(ender)
                # print("MSG to send -> Encoded by" , the_encoding)
                componentes = {
                    'header': self.header,
                    'page': complete_nro(this_page, n=new_n),
                    'tot_pages': complete_nro(hex_n_msgs, n=new_n),
                    'length': complete_nro(hextring(msg_len), n=new_n)
                }
                msg_header = head_template.format(**componentes)
                this_msg = NON.join([msg_header.encode(char_code),
                                     msg_i,
                                     msg_ender.encode(char_code)])
                #bprint("Generated msg Encoded by")
                # rprint(this_msg)
                yield this_msg

        elif n_msgs == 1:
            this_page = hextring(1)
            msg_len = (T_LEN + len(ender))
            hex_msg_len = complete_nro(hextring(msg_len), n=new_n)
            componentes = {
                'header': self.header,
                'page': complete_nro(this_page, n=new_n),
                'tot_pages': complete_nro(hextring(1), n=new_n),
                'length': complete_nro(hextring(msg_len), n=new_n)
            }
            msg_header = head_template.format(**componentes)
            this_msg = NON.join([msg_header.encode(char_code),
                                 b_msg,
                                 msg_ender.encode(char_code)])
            #print("Generated msg Encoded by" , the_encoding)
            yield this_msg

    def dumps_msg(self, msg):
        """
        Send a message, for example to send a serialized object
        """
        IDX = self.gen_idx().encode(char_code)
        for b_msg in self.generate_msg(msg):
            to_send = b"".join([IDX, b" ", b_msg])
            yield to_send

    def loads_msg(self, msg_list):
        """
        Receive the message controling pagination, buffsize, etc
        """

        bs_0 = 1
        count = 0
        b_header = b''
        idx_recv = ''
        t = True
        msg_tot = b''
        mlist = []
        n_msgs_idx = 0

        for elem in msg_list:
            #heartbeat = await self.heart_beat(id_client)        
            # addr = self.addr
            try:
                # moment 1 -> get hueader
                if writer.transport.is_closing():
                    # self.logger.error("La conexión se cerró %s" % writer)
                    raise Exception("Conexión perdida")
                idx_recv = ''
                page = [0, 0]
                while t:
                    while count < 4:
                        if not reader.at_eof():
                            char_recv = None
                            # moment 2 -> get content message, check if there are more
                            try:
                                char_recv = await shield(self.readbytes(reader, bs_0,
                                                                        id_client,origin="recv_msg_header"))
                            except asyncio.TimeoutError as te:
                                self.logger.exception("Tiempo fuera en intento de cerrar conexión %s, mode %s" %(
                                    te, self.mode))
                                await asyncio.sleep(10)
                                continue
                            except (ConnectionResetError, ConnectionAbortedError) as conn_error:
                                self.logger.exception("Excepción por desconexión %s, mode %s"%(
                                    conn_error, self.mode))
                                await asyncio.sleep(10)
                                continue
                            except Exception as ex:
                                self.report("recv_msg","Error al leer data -> %s" % ex, char_recv)
                                continue
                        else:
                            ex = reader.exception()
                            await asyncio.sleep(1)
                            print("Excepcion de lectura -> EOF")
                        b_header += char_recv
                        # cuando ocurre un espacio, sumar count
                        # de otra manera continuar iteración
                        if char_recv == b" ":
                            count += 1
                    header = b_header.decode(char_code)
                    b_header = b''
                    spaces = [x.start() for x in re.finditer(' ', header)]
                    sp_1 = spaces[0]
                    sp_2 = spaces[1]
                    sp_3 = spaces[2]
                    # obtener número de página
                    s_page = header[sp_2 + 1:sp_3].split("/")
                    a = page[0]
                    b = page[1]
                    page = list(map(hextring2int, s_page))
                    sp_4 = spaces[3]
                    check_header = header[sp_1 + 1:sp_2]
                    assert self.header == header[sp_1 + 1:
                                                 sp_2], "No es un encabezado correcto"
                    this_idx_recv = header[:sp_1]
                    if idx_recv == '':
                        idx_recv = this_idx_recv
                        n_msgs_idx = 1
                    else:
                        n_msgs_idx += 1
                    assert n_msgs_idx == page[
                        0], "Error en mensaje, no coincide #idx con pagina " + str(
                            page[0])
                    lmsg = header[sp_3 + 1:sp_4]
                    bs_1 = hextring2int(lmsg)+1
                    b_MSG = b''
                    n = 0
                    while n < bs_1:
                        bm = None
                        # moment 2 -> get content message, check if there are more
                        try:
                            bm = await shield(self.readbytes(reader, 1, id_client, origin="recv_msg_body"))
                        except asyncio.TimeoutError as te:
                            self.logger.exception("Tiempo fuera en intento de cerrar conexión %s, mode %s" %(
                                te, self.mode))
                            await asyncio.sleep(10)
                            continue
                        except (ConnectionResetError, ConnectionAbortedError) as conn_error:
                            self.logger.exception("Excepción por desconexión %s, mode %s"%(
                                conn_error, self.mode))
                            await asyncio.sleep(10)
                            continue                            
                        except Exception as ex:
                            self.logger.exception("Error al leer datos %s, con %s" %(ex, b_MSG))
                            print("Error en conexión excepction:<%s>" % ex)
                            print("Mensaje....", bm)
                            continue
                        if bm:
                            b_MSG += bm
                            n += 1
                    l_ender = len(self.ender.encode(char_code))
                    nlast = b_MSG.rfind(b" ")
                    pre_msg = b_MSG[:nlast]
                    count = 0
                    if pre_msg == b"<END>" or page[0] == page[1]:
                        t = False
                        mlist.append(pre_msg)
                        n_msgs_idx = 0
                        break
                    else:
                        mlist.append(pre_msg)
            except asyncio.CancelledError as ex:
                self.logger.exception("Error de conexión cancelada  %s" %ex)                        
                self.report("recv_msg","Asyncio cancelación de conexión <%s>"%ex)
            except socket.error as ex:
                self.logger.exception("Error de conexión de socket  %s" %ex)
                self.report("recv_msg","Error de socket")
                await asyncio.sleep(3)
                self.report("recv_msg","Falla en la conexión al recibir mensaje, %s"%ex)                
            except Exception as ex:
                await asyncio.sleep(3)
                self.logger.exception("Error de conexión  %s" %ex)            
                self.report("recv_msg","Falla en la conexión al recibir mensaje, %s"%ex)
                self.report("recv_msg","Lista msgs", mlist)
            #print("Procesando final")
            msg_tot = b"".join(mlist)
            MSG_TOT = msg_tot.decode(char_code)
            MT = MSG_TOT.strip()
            MT = MSG_TOT.rstrip()
            self.msg_r = MSG_TOT
            return MSG_TOT
        else:
            return None

    def gen_idx(self):
        IDX = my_random_string(self.uin)
        t = True
        while t:
            if IDX not in self.idx:
                self.idx.append(IDX)
                t = False
            else:
                IDX = my_random_string(self.uin)
        return IDX
