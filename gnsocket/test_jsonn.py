from datetime import datetime
from interpreter import JSONInterpreter
a={"a":1,"b":2}
a.update({"dt":datetime.utcnow()})
converter =  lambda e: e.isoformat()
jq = JSONInterpreter(converter)
r=jq.send(a)
print(r)
