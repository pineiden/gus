import socket
from basic_queuetools.queue import read_queue_gen, read_async_queue
from gnsocket.gn_socket import GNCSocket
# Standar lib
import asyncio
import functools
from multiprocessing import Manager, Queue, Lock

# contrib modules
import ujson as json

# Own module
from gnsocket.gn_socket import GNCSocket
from gnsocket.exceptions import clean_exception

# module tasktools
from tasktools.taskloop import coromask, renew, simple_fargs
from networktools.colorprint import gprint, bprint, rprint

from networktools.library import pattern_value, \
    fill_pattern, context_split, \
    gns_loads, gns_dumps
from networktools.library import my_random_string
from asyncio import shield

import ujson as json
import re
from networktools.path import home_path
from networktools.messages import MSGException, MessageLog

from dataclasses import field, dataclass
from .interpreter import Interpreter, JSONInterpreter

tsleep = 2

@dataclasses
class GNCSocketBase:
    queue_n2t: asyncio.Queue
    queue_t2n: asyncio.Queue
    queue_error: asyncio.Queue
    callback_exception: Callable[Any] =  field(default_factory=print)
    mode: str = "client"
    host: str = 'localhost'
    port: int = 6666
    client_name: str = "test"
    raise_timeout: bool = False 
    delta: float = .1
    interpreter: Interpreter = field(default_factory=lambda: JSONInterpreter())
    
    @property
    def address(self):
        return (self.host, self.port)

    async def sock_write(self, gs, *args, **kwargs):
        queue = self.qn2t
        await asyncio.sleep(self.delta)
        for idc in list(gs.clients.keys()):
            try:
                async for msg in read_async_queue(queue, fn_name='sock_write'):
                    msg_send = self.interpreter.send(msg)
                    idc_server = msg.get('idc_server')
                    try:
                        send_msg = gs.send_msg(msg_send, idc)
                        await send_msg
                    except BrokenPipeError as be:
                        self.report("sock_write","Close->Broken Pipe Error al cerrar %s bytes" %(be))
                        gs.logger.exception("Tiempo fuera en escritura %s, mode %s" %(
                            te, gs.mode))
                        await asyncio.sleep(1)
                        continue
                    except socket.error as se:
                        self.report("socke_write","Close->Socket Error al leer %s bytes" %(se))
                        await asyncio.sleep(1)
                        continue
                    except asyncio.TimeoutError as te:
                        gs.report("sock write", "Timeout error A", te)
                        gs.logger.exception("Tiempo fuera en escritura %s, mode %s" %(
                            te, gs.mode))
                        await asyncio.sleep(1)
                        continue
                    except (ConnectionResetError, ConnectionAbortedError) as conn_error:
                        gs.report("sock write", "Error de conexion", conn_error, gs.mode)
                        gs.logger.exception("Excepción por desconexión %s, mode %s"%(
                            conn_error,gs.mode))
                        await asyncio.sleep(1)
                        gs.set_status(False)
                        del gs.clients[idc]
                        continue
                    except Exception as ex:
                        gs.on_new_client(self.client_name)
                        gs.report("sock write", "Exepción", ex)
                        gs.logger.exception("Error con modulo cliente gnsocket coro write %s" %ex)
                        print("Error con modulo de escritura del socket IDC %s" % idc)
                        continue
            except asyncio.TimeoutError as te:
                gs.report("sock write", "Timeout error B",te)
                gs.logger.exception("HEARTBEAT: Tiempo fuera en escritura %s, mode %s" %(
                    te, gs.mode))
                await asyncio.sleep(10)
            except (ConnectionResetError, ConnectionAbortedError) as conn_error:
                gs.report("sock write", "Error de conexion", conn_error, gs.mode)
                gs.logger.exception("HEARTBEAT: Excepción por desconexión %s, mode %s"%(
                    conn_error,gs.mode))
                await asyncio.sleep(10)
                gs.set_status(False)
                del gs.clients[idc]
            except Exception as ex:
                gs.report("sock write", "Exepción", ex)
                gs.logger.exception("HEARTBEAT: Error con modulo cliente gnsocket coro write %s" %ex)
                gs.report("sock_write","Error con modulo de escritura del socket IDC %s" % idc)
        else:
            await asyncio.sleep(self.delta*10)
        return [gs, *args], kwargs
    # socket communication terminal to engine

    async def sock_read(self, gs, *args, **kwargs):
        queue_t2n = self.qt2n
        msg_from_engine = []
        await asyncio.sleep(self.delta)
        for idc in list(gs.clients.keys()):
            try:
                print("Recibiendo msg")
                recv_msg = gs.recv_msg(idc)
                datagram = await recv_msg
                if datagram not in {'', "<END>", 'null', None}:
                    msg_dict = self.interface.receive(datagram)
                    msg = {'dt': msg_dict, 'idc': idc}
                    await queue_t2n.put(msg)
            except BrokenPipeError as be:
                self.report("sock_read","Close->Broken Pipe Error al cerrar %s bytes" %(be))
                gs.logger.exception("Tiempo fuera en escritura %s, mode %s" %(
                    te, gs.mode))
                await asyncio.sleep(1)
                continue
            except socket.error as se:
                self.report("socke_read","Close->Socket Error al leer %s bytes" %(se))
                await asyncio.sleep(1)
                continue
            except asyncio.TimeoutError as te:
                gs.report("sock_read", "Timeout error A",te)
                gs.logger.exception("Tiempo fuera en escritura %s, mode %s" %(
                    te, gs.mode))
                await asyncio.sleep(2)
                continue
            except (ConnectionResetError, ConnectionAbortedError) as conn_error:
                gs.report("sock_read", "Error de conexion", conn_error, gs.mode)
                gs.logger.exception("Excepción por desconexión %s, mode %s"%(
                    conn_error, gs.mode))
                await asyncio.sleep(2)
                gs.set_status(False)
                del gs.clients[idc]
                if self.exception:
                    self.exception(conn_error, gs, idc)
                continue
            except Exception as ex:
                gs.report("sock_read", "Exepción", ex)
                rprint(ex)
                gs.logger.exception("Error con modulo cliente gnsocket coro read %s" %ex)           
                gs.report("sock_read","Some error %s en sock_read" % ex)
                continue
        else:
            await asyncio.sleep(self.delta*10)
            idc = self.client
        return [gs, *args], kwargs

    def set_socket_task(self, callback_socket_task):
        self.socket_task = callback_socket_task
