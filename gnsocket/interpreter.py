"""
Create the protocol
To translate bytes to object and vice versa
"""
from typing import Protocol, Any, Callable, Dict
from abc import abstractmethod
from dataclasses import field, dataclass

class Interpreter(Protocol):
    @abstractmethod
    def receive(self, msg_bytes:bytes) -> Any:
        pass 

    @abstractmethod
    def send(self, msg: Any) -> Any:
        pass

import ujson as json
"""
Create JSON Interprete
"""
@dataclass
class JSONInterpreter(Interpreter):
    converter: Callable = field(default_factory=lambda: str)

    def receive(self, msg_bytes:str) -> Dict[str, Any]:
        return json.loads(msg_bytes)

    def send(self, msg: Dict[str, Any]) -> str:
        return json.dumps(msg, default=self.converter)

    def set_converter(self, converter: Callable):
        self.converter = converter

"""
Create other inteprete
"""
